package Collections;

import java.util.*;

public class Verbs extends Words{
    // Data member variables
    Stack<String> verbsStack;

    // Member methods
    Verbs() {
        this.verbsStack = new Stack<String>();
    }

    // adds the specified string to the stack
    void addVerbs(String newVerb) {
        this.verbsStack.push(newVerb);
    }

    // gets the next string from the stack
    String getVerbs() {
        return (String)this.verbsStack.pop();
    }
}
