package Collections;

import java.util.*;


public class Adjectives extends Words{
    // Data member variables
    List adjectivesList;

    // Member methods
    Adjectives() {
        this.adjectivesList = new ArrayList();
    }

    // adds a new string to the ArrayList
    void addAdjectives(String newAdjective) {
        this.adjectivesList.add(newAdjective);
    }

    // gets the string from the list in the specified position
    String getAdjectives(int position) {
        return (String)this.adjectivesList.get(position);
    }
}
