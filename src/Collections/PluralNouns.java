package Collections;

import java.util.*;

public class PluralNouns extends Words{
    // Data member variables
    Queue pNounQueue;

    // Member methods
    PluralNouns() {
        this.pNounQueue = new PriorityQueue();
    }

    //adds the specifed string to the queue
    void addPluralNouns(String newPluralNoun) {
        this.pNounQueue.add(newPluralNoun);
    }

    //gets the next string from the queue
    String getPluralNouns() {
        return (String)this.pNounQueue.poll();
    }
}
