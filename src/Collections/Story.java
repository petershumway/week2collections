package Collections;

import java.util.*;

public class Story {

    // Data member variables
    private int numAdjectives;
    private int numNouns;
    private int numParts;
    private int numPluralNouns;
    private int numVerbs;
    private int numNumbers;
    private Map storyOrderMap;
    private Adjectives storyAdjectives;
    private Nouns storyNouns;
    private PluralNouns storyPluralNouns;
    private Parts storyBodyParts;
    private Verbs storyVerbs;
    private Number storyNumber;


    // Member methods

    // constructor
    Story() {
        this.numAdjectives = 4;
        this.numNouns = 4;
        this.numParts = 2;
        this.numPluralNouns = 4;
        this.numVerbs = 2;
        this.numNumbers = 1;
        this.storyOrderMap = new HashMap();
        this.storyAdjectives = new Adjectives();
        this.storyNouns = new Nouns();
        this.storyPluralNouns = new PluralNouns();
        this.storyBodyParts = new Parts();
        this.storyVerbs = new Verbs();
        this.storyNumber = new Number();

    }

    // getter methods
    public int getNumAdjectives() {
        return this.numAdjectives;
    }

    public int getNumNouns() {
        return this.numNouns;
    }

    public int getNumParts() {
        return this.numParts;
    }

    public int getNumPluralNouns() {
        return this.numPluralNouns;
    }

    public int getNumVerbs()  {
        return this.numVerbs;
    }

    public int getNumNumbers() {
        return this.numNumbers;
    }

    public void obtainAdjectives() {
        for (int i = 0; i < getNumAdjectives(); i++) {
            this.storyAdjectives.addAdjectives(this.storyAdjectives.getUserInput("Adjective", i + 1));
        }
    }

    public void obtainNouns() {
        for (int i = 0; i < getNumNouns(); i++) {
            this.storyNouns.addNouns(this.storyNouns.getUserInput("Noun", i + 1));
        }
    }

    public void obtainPluralNouns() {
        for (int i = 0; i < getNumPluralNouns(); i++) {
            this.storyPluralNouns.addPluralNouns(this.storyPluralNouns.getUserInput("Plural Noun", i + 1));
        }
    }

    public void obtainBodyParts() {
        for (int i = 0; i < getNumParts(); i++) {
            this.storyBodyParts.addBodyParts(this.storyBodyParts.getUserInput("Body Part", i + 1));
        }
    }

    public void obtainVerbs() {
        String verbType;
        for (int i = 0; i < getNumVerbs(); i++) {
            if (i == 0){
                verbType = "Verb ending with 'ing'";
            }
            else {
                verbType = "Verb";
            }
            this.storyVerbs.addVerbs(this.storyVerbs.getUserInput(verbType, i + 1));
        }
    }
    public void obtainNumber() {
        for (int i = 0; i < getNumNumbers(); i++) {
            this.storyNumber.addNumber(this.storyNumber.getUserInput("Number", i + 1));
        }
    }


    void fillStory() {

        // Fill the adjectives
        String[] aArray = new String[getNumAdjectives()];

        for (int i = 0; i < getNumAdjectives(); i++) {
            aArray[i] = storyAdjectives.getAdjectives(i);
        }
        // fill the map with the correct order
        storyOrderMap.put(6, aArray[0]);
        storyOrderMap.put(9, aArray[1]);
        storyOrderMap.put(12, aArray[2]);
        storyOrderMap.put(14, aArray[3]);


        // Fill the Nouns
        String[] nArray = new String[getNumNouns()];

        for (int i = 0; i < getNumNouns(); i++) {
            nArray[i] = storyNouns.getNouns();
        }
        // fill the map with the correct order
        storyOrderMap.put(2, nArray[0]);
        storyOrderMap.put(3, nArray[1]);
        storyOrderMap.put(5, nArray[2]);
        storyOrderMap.put(16, nArray[3]);


        // fill the Number
        int[] numArray = new int [getNumNumbers()];

        for (int i = 0; i < getNumNumbers(); i++) {
            numArray[i] = storyNumber.getNumber(i);
        }
        // fill the map with the correct order
        storyOrderMap.put(15, numArray[0]);


        // Fill the Parts of body
        String[] pArray = new String[getNumParts()];

        for (int i = 0; i < getNumParts(); i++) {
            pArray[i] = storyBodyParts.getBodyParts(i);
        }
        // fill the map with the correct order
        storyOrderMap.put(7, pArray[0]);
        storyOrderMap.put(10, pArray[1]);

        // Fill the plural nouns
        String[] pNArray = new String[getNumPluralNouns()];

        for (int i = 0; i < getNumPluralNouns(); i++) {
            pNArray[i] = storyPluralNouns.getPluralNouns();
        }
        // fill the map with the correct order
        storyOrderMap.put(8, pNArray[0]);
        storyOrderMap.put(11, pNArray[1]);
        storyOrderMap.put(13, pNArray[2]);
        storyOrderMap.put(17, pNArray[3]);

        // fill the Verbs
        String[] vArray = new String[getNumVerbs()];

        for (int i = 0; i < getNumVerbs(); i++) {
            vArray[i] = storyVerbs.getVerbs();
        }
        // fill the map with the correct order
        storyOrderMap.put(1, vArray[0]);
        storyOrderMap.put(4, vArray[1]);

    }

    void displayStory() {

        // the story displays with each of the user input details added in the right place. The user input details
        // print red on the console.
        System.out.println();
        System.out.println("I love to "+(char)27+"[31m"+(String)storyOrderMap.get(1)+(char)27 +"[30m video games." +
                " I can play them day and ");

        System.out.println((char)27+"[31m"+(String)storyOrderMap.get(2)+(char)27+"[30m! My mom and " +
                (char)27+"[31m"+(String)storyOrderMap.get(3)+(char)27 +"[30m are not too happy with my ");

        System.out.println((char)27+"[31m"+(String)storyOrderMap.get(4)+(char)27+"[30m so much time in front of" +
                " the television "+(char)27+"[31m"+(String)storyOrderMap.get(5)+(char)27+"[30m.");

        System.out.println("Although Dad believes that these "+(char)27+"[31m"+(String)storyOrderMap.get(6)+
                (char)27+"[30m games help children ");

        System.out.println("develop hand-"+(char)27+"[31m"+(String)storyOrderMap.get(7)+(char)27+"[30m " +
                "coordination and improve their ");

        System.out.println("learning "+(char)27+"[31m"+(String)storyOrderMap.get(8)+(char)27+"[30m, he also " +
                "seems to think they have "+(char)27+"[31m"+(String)storyOrderMap.get(9)+(char)27+"[30m");

        System.out.println("side effects on one's "+(char)27+"[31m"+(String)storyOrderMap.get(10)+(char)27+"[30m"+
                ". Both of my "+(char)27+"[31m"+(String)storyOrderMap.get(11)+(char)27+"[30m");

        System.out.println("think this is due to a "+(char)27+"[31m"+(String)storyOrderMap.get(12)+(char)27+"[30m"+
                " use of violence in the majority");

        System.out.println("of the "+(char)27+"[31m"+(String)storyOrderMap.get(13)+(char)27 +"[30m. Finally, we " +
                "all arrived at a "+(char)27+"[31m"+(String)storyOrderMap.get(14)+(char)27+"[30m");

        System.out.println("compromise: After dinner I can play "+(char)27+"[31m"+(int)storyOrderMap.get(15)+
                (char)27+"[30m hours of video games,");

        System.out.println("provided I help clear the "+(char)27+"[31m"+(String)storyOrderMap.get(16)+(char)27+"[30m"+
                " and wash the "+(char)27+"[31m"+(String)storyOrderMap.get(17)+(char)27+"[30m.");

        System.out.println();

    }

}
