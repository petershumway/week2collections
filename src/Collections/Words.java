package Collections;

import java.util.InputMismatchException;
import java.util.Scanner;

public abstract class Words {

    //Data member variables

    //Member methods

    String getUserInput(String type, int number) {
        // Create Scanner object for input
        Scanner input = new Scanner(System.in);

        boolean continueInput;
        String newInput = "error";

        do {
            // error handling for mismatched input
            try {
                System.out.println("Please enter " + type + " #" + number + ": ");

                newInput = input.next();

                // as long as there was no exception continue to the next input
                continueInput = false;
            }
            // catch any input mismatch errors
            catch (InputMismatchException ex) {
                // display the error message
                System.out.println("Error: The input must be text characters or numbers.");
                // clear input
                input.nextLine();
                // make sure the loop will happen again for the current string
                continueInput = true;
            }
        } while (continueInput);

        return newInput;
    }
}
