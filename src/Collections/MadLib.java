// this program is intended to show the use of various different types of collections in java.
// different classes house words in different types of collections to show the basic functionality of those collections
// and show how they can work similarly but with different syntax.

package Collections;

import java.util.*;

public class MadLib {

    public static void main(String[] args) {

        //Introduction message and instructions
        System.out.println("Video Games MadLib");
        System.out.println();
        System.out.println("In this game you are asked for some words and a number which are then put into a story.");
        System.out.println("When you have entered all the required details a silly story will be shown for you to " +
                "enjoy.");
        System.out.println();
        System.out.println("You will be asked for 4 Nouns, 4 Plural Nouns, 4 Adjectives, 2 Verbs, " +
                "2 Parts of the body, and 1 number.");
        System.out.println();

        // Create a new story, fill the variables, bring the variables together in the correct order and display
        // the story
        Story story = new Story();

        // Get user input
        story.obtainAdjectives();    // Adjectives are housed in an ArrayList
        story.obtainNouns();         // Nouns are housed in a TreeSet
        story.obtainPluralNouns();   // PluralNouns are housed in a Queue
        story.obtainBodyParts();     // Parts are housed in a Vector
        story.obtainVerbs();         // Verbs are housed in a Stack
        story.obtainNumber();        // Number is housed in an ArrayList

        story.fillStory();           // The variables are stored in a Map before printing out.
        story.displayStory();

        //Goodbye and credits
        System.out.println("Thank You for playing! Credit to redkid.net for providing the story.");
        System.out.println("http://www.redkid.net/cgi-bin/madlibs/videogames.pl");
    }
}
