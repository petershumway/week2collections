package Collections;

import java.util.*;

public class Number {
    // Data member variables
    List numberList;

    // Member methods
    Number() {
        this.numberList = new ArrayList();
    }

    // adds a new int to the ArrayList
    void addNumber(int newNumber) {
        this.numberList.add(newNumber);
    }

    // gets the int from the list in the specified position
    int getNumber(int position) {
        return (int)this.numberList.get(position);
    }

    int getUserInput(String type, int number) {

        // Create Scanner object for input
        Scanner input = new Scanner(System.in);

        boolean continueInput;
        int newInput = 0;

        do {
            // error handling for mismatched input
            try {
                System.out.println("Please enter " + type + " #" + number + ": ");

                newInput = input.nextInt();

                // as long as there was no exception continue to the next input
                continueInput = false;
            }
            // catch any input mismatch errors
            catch (InputMismatchException ex) {
                // display the error message
                System.out.println("Error: The input must be an integer made up of numbers 0-9.");
                // clear input
                input.nextLine();
                // make sure the loop will happen again for the current integer
                continueInput = true;
            }
        } while (continueInput);

        return newInput;
    }
}

