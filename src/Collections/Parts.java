package Collections;

import java.util.*;

public class Parts extends Words{
    // Data member variables
    Vector<String> partsVector;

    // Member methods
    Parts() {
        partsVector = new Vector<String>();
    }

    // adds the specified string to the Vector
    void addBodyParts(String newBodyPart) {
        this.partsVector.add(newBodyPart);
    }

    // gets the next string from the vector
    String getBodyParts(int position) {
        return this.partsVector.elementAt(position);
    }
}
