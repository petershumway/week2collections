package Collections;

import java.util.*;

public class Nouns extends Words{
    // Data member variables
    TreeSet nounSet;

    // Member methods
    Nouns() {
        this.nounSet = new TreeSet();
    }

    // adds a new string to the TreeSet
    void addNouns(String newNoun) {
        this.nounSet.add(newNoun);
    }

    // gets the next string from the TreeSet
    String getNouns() {
        String firstNoun = (String)this.nounSet.first();
        this.nounSet.remove(firstNoun);

        return firstNoun;
    }

    @Override
    String getUserInput(String type, int number) {
        // Create Scanner object for input
        Scanner input = new Scanner(System.in);

        boolean continueInput;
        String newInput = "error";

        do {
            // error handling for mismatched input
            try {
                System.out.println("Please enter " + type + " #" + number + ": ");

                newInput = input.next();

                // as long as there was no exception continue to the next input
                continueInput = false;

                for (Object check : this.nounSet) {
                    if (newInput.equals(check)) {
                        throw new Exception();
                    }
                }

            }
            // catch any input mismatch errors
            catch (InputMismatchException ex) {
                // display the error message
                System.out.println("Error: The input must be text characters or numbers.");
                // clear input
                input.nextLine();
                // make sure the loop will happen again for the current string
                continueInput = true;
            }

            catch (Exception e) {
                // display the error message
                System.out.println("Error: You cannot enter the same Noun more than once.");
                // clear input
                input.nextLine();
                // make sure the loop will happen again for the current string
                continueInput = true;
            }
        } while (continueInput);


        return newInput;
    }
}
